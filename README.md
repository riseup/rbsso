# rbsso

## Installation

Before installing this gem, make sure you have `libsodium` installed on your
system.

On Debian Bookworm:

```bash
apt install libsodium23
```

For now, the way to use this gem is to installed it from this repository, so in
your `Gemfile` you're going to add something like this:

```ruby
gem 'rbsso', git: 'https://0xacab.org/liberate/rbsso', tag: '0.5.0'
```
